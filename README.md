# NFC instrument

This will play sinewaves up, down and stop by scanning at least three NFC cards. Runs on Linux. Hacked by Linse and Samuel.

## Setup

* Plug in the badge [NFC reader from BornHack 2023](https://github.com/bornhack/badge2023)
* Change the `sleep` command to `sleep(0.1)` in `code.py`.

Setup the program

```
virtualenv venv -p python3
source venv/bin/activate
pip install -r requirements.txt
python player.py
```

Now have at least 3 NFC badges/cards/tags and scan each one. The first one will be given the operation pitch up, the second pitch down and the third stop. If more badges are scanned they will be given operations in the same order.

## Resources

* [Python serial read](https://www.pythonpool.com/python-serial-read/)
* [pysinewave](https://pypi.org/project/pysinewave/)