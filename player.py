from enum import Enum
import serial
from pysinewave import SineWave

addr = "/dev/ttyACM0"
# Create a sine wave, with a starting pitch of 6, and a pitch change speed of 10/second.
pitch = 6
sinewave = SineWave(pitch = pitch, pitch_per_second = 10)

known_cards = {}

PITCH_UP = 0
PITCH_DOWN = 1
STOP = 2
operations = [
    PITCH_UP,
    PITCH_DOWN,
    STOP
]

current_operation = -1
def get_next_operation():
    global current_operation
    current_operation += 1
    if current_operation >= len(operations):
        current_operation = 0
    return operations[current_operation]

card_pitch_up = True
note_running = False
with serial.Serial(addr, 3443, timeout=10) as serial:
    while True:
        line = serial.readline()
        line = line.decode()
        
        if "ID" not in line:
            continue
        print("line")
        print(line)
        parts = line.split("ID:")
        id = parts[1].strip()
        print("ID is {}".format(id))
        if id in known_cards:
            operation = known_cards[id]
            print("Known card of {}, operation: {}".format(id, operation))
        else:
            print("New card")
            new_op = get_next_operation()
            print("new op is {}".format(new_op))
            known_cards[id] = new_op
            operation = new_op
        
        if operation == PITCH_UP:
            pitch += 1
            sinewave.set_pitch(pitch)
            sinewave.play()
        elif operation == PITCH_DOWN:
            pitch -= 1
            sinewave.set_pitch(pitch)
            sinewave.play()
        elif operation == STOP:
            sinewave.stop()

